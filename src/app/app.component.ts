import { Component, OnDestroy, OnInit } from '@angular/core';
import { ReplaySubject, BehaviorSubject, Subject } from 'rxjs';
import {
  IpEmailBuilderService,
  Structure,
  TextBlock,
  IPEmail,
} from 'ip-email-builder';
import { exhaustMap, takeUntil } from "rxjs/operators";
import { ApiServiceService } from "./api-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isLoading: ReplaySubject<boolean>;
  private componentDestroyed$ = new Subject();


  email = new BehaviorSubject(
    new IPEmail({
      structures: [
        new Structure('cols_1', [[new TextBlock("I'm a new email")]]),
      ],
    })
  );
 
  constructor(
    private ngb: IpEmailBuilderService,
    private apiService: ApiServiceService
    ) {
    this.isLoading = ngb.isLoading;
    ngb.MergeTags = new Set(['{{firstName}}', '{{lastName}}']);
  }
  ngOnDestroy(): void {
  }
  ngOnInit() {
    this.ngb.onSave$
      .pipe(
        exhaustMap(({ email, template }) =>
          this.apiService.saveEmail(email, template)
        ),
        takeUntil(this.componentDestroyed$)
      )
      .subscribe(() => console.log('Email and Template saved.'));
  }
 
  changeEmail() {
    this.email.next(
      new IPEmail({
        structures: [
          new Structure('cols_1', [
            [new TextBlock("I'm a new block created via subscribtion")],
          ]),
        ],
      })
    );
  }
}
