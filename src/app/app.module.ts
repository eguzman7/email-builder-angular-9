import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IpEmailBuilderModule } from 'ip-email-builder';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IpEmailBuilderModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
